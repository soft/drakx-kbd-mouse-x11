package Xconfig::screen; # $Id: screen.pm 274851 2012-08-17 08:40:12Z peroyvind $

use diagnostics;
use strict;

use common;
# perl_checker: require Xconfig::xfree

sub configure {
    my ($raw_X) = @_; # perl_checker: $raw_X = Xconfig::xfree->new

    my @devices = $raw_X->get_devices;
    my @monitors = $raw_X->get_monitors;

    if (@monitors < @devices) {
	$raw_X->set_monitors(@monitors, ({}) x (@devices - @monitors));
	@monitors = $raw_X->get_monitors;
    }

    my @sections = mapn {
	my ($device, $monitor) = @_;
	{ Device => $device->{Identifier}, Monitor => $monitor->{Identifier} };
    } \@devices, \@monitors;

    $raw_X->set_screens(@sections);
    1;
}

1;
