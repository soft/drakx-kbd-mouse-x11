package Xconfig::main; # $Id: main.pm 274852 2012-08-17 08:40:27Z peroyvind $




use Xconfig::monitor;
use Xconfig::card;
use Xconfig::plugins;
use Xconfig::resolution_and_depth;
use Xconfig::various;
use Xconfig::screen;
use Xconfig::test;
use Xconfig::xfree;
use common;


sub configure_monitor
{ # not used by XFdrake itself
    my ($in) = @_;

    my $raw_X = Xconfig::xfree->read;
    if (!defined($raw_X)) {
	print STDERR "Error: No X configuration found.\n";
	return 0;
    }
    Xconfig::monitor::configure($in, $raw_X, int($raw_X->get_devices)) or return;
    if ($raw_X->is_modified)
    {
        $raw_X->write;
        return 'need_restart';
    }
    else {
        return '';
    }
}

sub configure_resolution {
    my ($in) = @_;

    my $raw_X = Xconfig::xfree->read;
    if (!defined($raw_X)) {
	print STDERR "Error: No X configuration found.\n";
	return 0;
    }
    my $X = { 
		card => Xconfig::card::from_raw_X($raw_X),
		monitors => [ $raw_X->get_monitors ],
    };

    $X->{resolutions} = Xconfig::resolution_and_depth::configure($in, $raw_X, $X->{card}, $X->{monitors}) or return;
    if ($raw_X->is_modified) {
		return &write($raw_X, $X, {});
    }
    else {
		return '';
    }
}


sub configure_everything_auto_install {
    my ($raw_X, $do_pkgs, $old_X, $options) = @_;
    my $X = {};
    $X->{monitors} = Xconfig::monitor::configure_auto_install($raw_X, $old_X) or return;
    $options->{VideoRam_probed} = $X->{monitors}[0]{VideoRam_probed};
    $X->{card} = Xconfig::card::configure_auto_install($raw_X, $do_pkgs, $old_X, $options);
    my $configured = $X->{card} && Xconfig::screen::configure($raw_X);

    #- still try to configure screen and resolution (to set default background)
    #- if card configuration failed (for example if best driver is not available)
    $X->{resolutions} = Xconfig::resolution_and_depth::configure_auto_install($raw_X, $X->{card} || {}, $X->{monitors}, $old_X);
    return if !$configured;

    return &write($raw_X, $X, $options);
}

sub configure_everything {
    my ($in, $raw_X, $do_pkgs, $auto, $options) = @_;
    my $X = {};
    my $ok = 1;

    my $probed_info = Xconfig::monitor::probe();
    $options->{VideoRam_probed} = $probed_info->{VideoRam_probed};
    $ok &&= $X->{card} = Xconfig::card::configure($in, $raw_X, $do_pkgs, $auto, $options);
    if(not $options->{freedriver})
    {
        $ok &&= $X->{monitors} = Xconfig::monitor::configure($in, $raw_X, int($raw_X->get_devices), $probed_info, $auto);
        $ok &&= Xconfig::screen::configure($raw_X);
    }
    
    if($X->{monitors} and $X->{monitors}[0]{VendorName} ne "Plug'n Play" and not $options->{freedriver})
    { # do not configure resolution for pnp and free driver
        $ok &&= $X->{resolutions} = Xconfig::resolution_and_depth::configure($in, $raw_X, $X->{card}, $X->{monitors}, $auto, {});
    }
#    $ok &&= Xconfig::test::test($in, $raw_X, $X->{card}, '', 'skip_badcard') if !$auto;
    
    if (!$ok) {
	return if $auto;
	($ok) = configure_chooser_raw($in, $raw_X, $do_pkgs, $options, $X, 1);
    }
    may_write($in, $raw_X, $X, $ok, $options);
}

sub detect_current_driver()
{
    my $lsmod = `lsmod`;
    
    foreach my $d ("nouveau", "radeon", "nvidia", "amdgpu", "i915")
    {
        if($lsmod=~/\b$d\s+\d+\s+(\d+)/)
        { # nouveau 1160805 2
            if($1)
            {
                return $d;
            }
        }
    }
    
    return "vesa";
}

sub configure_chooser_raw
{
    my ($in, $raw_X, $do_pkgs, $options, $X, $b_modified) = @_;
    
    if($options and not $options->{'video_profile'}) {
        $options->{'video_profile'} = Xconfig::card::probe_video_cards(Xconfig::card::list_cards());
    }
    
    my $curdriver = detect_current_driver();
    
    my %texts;
    
    my $update_texts = sub
    {
        $texts{card} = $X->{card} && $X->{card}{BoardName} || N("Custom");
        
        my $dr = $X->{card}{Driver};
        
        if(not $dr)
        {
            my %XFdrake = MDK::Common::System::getVarsFromSh("/etc/XFdrake.conf");
            
            if(defined $XFdrake{"driver"}) {
                $dr = $XFdrake{"driver"};
            }
        }
        
        if(not $dr) {
            $dr = $curdriver;
        }
        
        if($dr)
        {
            if(Xconfig::card::is_free_driver($dr))
            { # detected free driver, not selected yet
                $options->{freedriver} = 1;
            }
        }
        
        if($dr) {
            $texts{card} = Xconfig::card::get_setup_profile($dr, $options);
        }
        else {
            $texts{card} = N("Custom");
        }
        
        if($options->{freedriver})
        {
            $texts{monitors} = "Plug'n Play";
            $texts{resolutions} = N("Automatic");
            $texts{options} = N("Automatic");
        }
        else
        {
            $texts{monitors} = $X->{monitors} && $X->{monitors}[0]{ModelName} || N("Custom");
            if($X->{monitors} and $X->{monitors}[0]{VendorName} eq "Plug'n Play") {
                $texts{monitors} = "Plug'n Play";
            }
            if($#{$X->{resolutions}}>=0) {
                $texts{resolutions} = Xconfig::resolution_and_depth::to_string($X->{resolutions}[0]);
            }
            else {
                $texts{resolutions} = N("Custom");
            }
            $texts{options} = N("Custom");
        }
        
        $texts{$_} =~ s/(.{40}).*/$1.../ foreach keys %texts; #- ensure not too long
    };
    
    $update_texts->($options);

    my $may_set; 
    my $prompt_for_resolution = sub {
        $may_set->('resolutions', Xconfig::resolution_and_depth::configure($in, $raw_X, $X->{card}, $X->{monitors}));
    };
    $may_set = sub
    {
        my ($field, $val) = @_;
        if($val)
        {
            $X->{$field} = $val;
            $X->{"modified_$field"} = 1;
            $b_modified = 1;
            
            $update_texts->();
            
            if(not $options->{freedriver})
            {
                if($field eq "monitors")
                {
                    if(not $raw_X->get_Section("Screen"))
                    { # where is no "Screen" section in config (pnp)
                        Xconfig::screen::configure($raw_X);
                        $X->{resolutions} = [ eval { $raw_X->get_resolutions } ];
                        $prompt_for_resolution->();
                    }
                    
                    if($X->{monitors}[0]{VendorName} eq "Plug'n Play")
                    { # set to pnp: set automatic resolution
                        $raw_X->remove_Section("Screen");
                        $may_set->('resolutions', Xconfig::resolution_and_depth::set_resolution($raw_X, $X->{card}, $X->{monitors}, {"automatic"=>1}, ()));
                        return;
                    }
                }
                
                if(member($field, 'card', 'monitors'))
                {
                    if($#{$X->{resolutions}}>=0)
                    {
                        my ($default_resolution, @other_resolutions) = Xconfig::resolution_and_depth::choices($raw_X, $X->{resolutions}[0], $X->{card}, $X->{monitors});
                        
                        if(Xconfig::resolution_and_depth::to_string($default_resolution) ne Xconfig::resolution_and_depth::to_string($X->{resolutions}[0])) {
                            $prompt_for_resolution->();
                        }
                        else
                        {
                            Xconfig::screen::configure($raw_X);
                            $may_set->('resolutions', Xconfig::resolution_and_depth::set_resolution($raw_X, $X->{card}, $X->{monitors}, $default_resolution, @other_resolutions));
                        }
                    }
                }
            }
        }
    };
    
    my $ok;
    $in->ask_from_({ interactive_help_id => 'configureX_chooser',
        title => N("Graphic Card & Monitor Configuration"), 
        if_($::isStandalone, ok => N("Apply settings")) }, 
        [
            { label => N("Video Driver"), val => \$texts{card}, clicked => sub { 
                $may_set->('card', Xconfig::card::configure($in, $raw_X, $do_pkgs, 0, $options));
            } },
            { label => N("_: This is a display device\nMonitor"), val => \$texts{monitors}, clicked => sub {
                $may_set->('monitors', Xconfig::monitor::configure($in, $raw_X, int($raw_X->get_devices)));
                }, disabled => sub {$options->{freedriver}} },
            { label => N("Resolution"), val => \$texts{resolutions}, disabled => sub { !$X->{card} || !$X->{monitors} },
                clicked => $prompt_for_resolution, disabled => sub {$options->{freedriver}} },
#           if_(Xconfig::card::check_bad_card($X->{card}) || $::isStandalone,
#               { val => N("Test"), disabled => sub { !$X->{card} || !$X->{monitors} },
#               clicked => sub { 
#               $ok = Xconfig::test::test($in, $raw_X, $X->{card}, 'auto', 0);
#               } },
#           ),
            { label => N("Options"), val => \$texts{options}, clicked => sub {
                Xconfig::various::various($in, $raw_X, $X->{card}, $options, 0, 'read_existing');
                Xconfig::card::to_raw_X($X->{card}, $raw_X);
                }, disabled => sub {$options->{freedriver}} },
            if_(Xconfig::plugins::list(), 
                { val => N("Plugins"), clicked => sub {
                Xconfig::plugins::choose($in, $raw_X);
                } }
            )
        ]);
    $ok, $b_modified;
}

sub configure_chooser
{
    my ($in, $raw_X, $do_pkgs, $options) = @_;

    my $X = {
        card => scalar eval { Xconfig::card::from_raw_X($raw_X) },
        monitors => [ $raw_X->get_monitors ],
        resolutions => [ eval { $raw_X->get_resolutions } ],
    };
    
    if(not $X->{monitors} or not @{$X->{monitors}})
    { # pnp by default
        $X->{monitors} = [{"VendorName" => "Plug'n Play"}];
        
        $raw_X->set_monitors(({
            "VendorName" => "Plug'n Play"
        }));
    }
    
    my ($ok) = configure_chooser_raw($in, $raw_X, $do_pkgs, $options, $X);

    if ($raw_X->is_modified) {
        return may_write($in, $raw_X, $X, $ok, $options);
    }
    
    return '';
}

sub configure_everything_or_configure_chooser
{
    my ($in, $options, $auto, $o_keyboard, $o_mouse) = @_;

    my $raw_X = eval { Xconfig::xfree->read };
    my $err = $@ && formatError($@);
    $err ||= _check_valid($raw_X) if $raw_X; #- that's ok if config is empty
    if ($err)
    {
        log::l("ERROR: bad X config file (error: $err)");
        $options->{ignore_bad_conf} or $in->ask_okcancel('', N("Your Xorg configuration file is broken, we will ignore it.")) or return;
        undef $raw_X;
    }
    
    # start X11 automatically
    # Xconfig::various::runlevel(5);

    my $rc = 'ok';
    if (!$raw_X)
    {
        $raw_X = Xconfig::default::configure($in->do_pkgs, $o_keyboard, $o_mouse);
        $rc = configure_everything($in, $raw_X, $in->do_pkgs, $auto, $options);
    } elsif (!$auto) {
        $rc = configure_chooser($in, $raw_X, $in->do_pkgs, $options);
    }
    $rc && $raw_X, $rc;
}


sub may_write {
    my ($in, $raw_X, $X, $ok, $options) = @_;
    
    if($X->{card}{Driver})
    { # driver should be selected
        $ok ||= $in->ask_yesorno('', N("Keep the changes?\nThe current configuration is:\n\n%s",
        Xconfig::various::info($raw_X, $X->{card})), 1);
        
        $ok && &write($raw_X, $X, $options);
    }
}

sub write
{
    my ($raw_X, $X, $options) = @_;
    if($::isInstall) {
		export_to_install_X($X);
	}
    my $only_resolution = $raw_X->is_only_resolution_modified;
    
    my $driver = $raw_X->get_Driver;
    
    if($options->{'video_profile'} eq "Intel+Nvidia"
    and $driver=~/\A(nvidia|nouveau)\Z/)
    { # 1. do not generate xorg.conf
      # 2. setup bumblebee.conf
        
        unlink(Xconfig::xfree::_conf_file());
        unlink(Xconfig::xfree::_monitor_conf_file());
        
        my $bb_conf_p = "/etc/bumblebee/bumblebee.conf";
        if(-e $bb_conf_p)
        {
            my $bb_conf = cat_($bb_conf_p);
            
            my $bb_conf_new = "";
            
            my %bb_vals =
            (
                "bumblebeed" => {
                    "KeepUnusedXServer" => "true",
                    "TurnCardOffAtExit" => "false",
                    "Driver" => $driver
                },
                "driver-nvidia" => {
                    "KernelDriver" => "nvidia", # alias to current nvidia driver (nvidia340, etc.)
                    "PMMethod" => "auto"
                },
                "driver-nouveau" => {
                    "KernelDriver" => "nouveau",
                    "PMMethod" => "auto"
                },
            );
            
            my $bb_sect = "";
            my $chg = 0;
            
            foreach my $line (split(/\n/, $bb_conf))
            {
                if($line=~/\A\[/)
                {
                    if($line=~/\[(.+)\]/) {
                        $bb_sect = $1;
                    }
                }
                else
                {
                    if($line=~/\A(.+)\=(.*)\Z/)
                    {
                        my ($k, $v) = ($1, $2);
                        
                        if($k eq "Driver")
                        {
                            if($v ne $driver)
                            {
                                $bb_conf_new .= "Driver=$driver\n";
                                $chg = 1;
                                next;
                            }
                        }
                        
                        if(my $bb_v = $bb_vals{$bb_sect}{$k})
                        {
                            if($bb_v ne $v)
                            {
                                $bb_conf_new .= $k."=".$bb_v."\n";
                                $chg = 1;
                                next;
                            }
                        }
                    }
                }
                
                $bb_conf_new .= $line."\n";
            }
            
            if($chg)
            {
                output($bb_conf_p, $bb_conf_new);
            }
        }
	}
	else
	{ # check for KMS is in xfree.pm
		$raw_X->write;
	}
    
    Xconfig::various::check_xorg_conf_symlink();
    Xconfig::various::setup_kms($raw_X, $options);
    
    MDK::Common::System::addVarsInSh("/etc/XFdrake.conf", {driver => $driver}); # save configuration
    
    if ($X->{resolutions} and $X->{resolutions}[0]{bios})
    {
		if(not $options->{skip_fb_setup}) {
			Xconfig::various::setupFB($X->{resolutions}[0]{bios});
		}
		return 'need_reboot';
    }
    elsif (my $resolution = $only_resolution && eval { $raw_X->get_resolution })
    {
		return 'need_xrandr' . sprintf(' --size %dx%d', @$resolution{'X', 'Y'});
    }
    else
    {
		# Always ask for reboot as the driver could've changed.
		# TODO: Do 'need_restart' instead when no reboot needed, like no driver
		# change.
		return 'need_reboot';
    }
}


sub export_to_install_X {
    my ($X) = @_;

    my $resolution = $X->{resolutions}[0];
    $::o->{X}{resolution_wanted} = $resolution->{automatic} ? 'automatic' : $resolution->{X} . 'x' . $resolution->{Y};
    $::o->{X}{default_depth} = $resolution->{Depth} if $resolution->{Depth};
    $::o->{X}{bios_vga_mode} = $resolution->{bios} if $resolution->{bios};
    $::o->{X}{monitors} = $X->{monitors} if $X->{monitors}[0]{manually_chosen} && $X->{monitors}[0]{vendor} ne "Plug'n Play";
    $::o->{X}{card} = $X->{card} if $X->{card}{manually_chosen};
    $::o->{X}{Xinerama} = 1 if $X->{card}{Xinerama};
}

sub _check_valid {
    my ($raw_X) = @_;

#     my %_sections = map { 
#         my @l = $raw_X->get_Sections($_) or return "missing section $_";
#         $_ => \@l;
#     } qw(ServerLayout);

    return '';
}

1;
